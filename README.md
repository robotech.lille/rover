# PROJET ROVER

Bienvenue sur le projet rover de robotech hâte de vous voir rejoindre cettre grande aventure !!
Ce projet n'est pas destiner seulement au nerd ! Il est destiner a qui le veux peut importe le niveau . Vous n'avez pas besoin de connaitre l'éléctronique , la programmation , la modelisation ... .

Lors de nos differente sceance nous apprendrons a utiliser les differents logiciel et travaillierons ensemble a la realisation de ce projet de manière progressive.

## Les objectifs

L'objectif de ce projet et de réaliser un rover modulable sur le quelle nous pourrons ajouter differents module que nous aurons imaginer comme une module service de bière.

La realisation de ce projet vous permettra d'augmenter vos competance en : Programation,  Electronique , Modelisation 3D , Gestion de projet et enfin un max de fun.

Le porjet s'inspire d'un projet realiser par How To Mechatronics : https://www.youtube.com/watch?v=NOZZMsMAGh0

## Les logiciels

Pour réaliser la modelisation des differentes pièces nous avons décidé d'utiliser fusion 360 logiciel de autodesk qui est gratuit pour les etudiant en utilisant votre adresse mail polytech :
https://www.autodesk.fr/education/edu-software/overview?sorting=featured&page=1

Pour la programmation vous pouvais utilisé arduino.ide qui utilise le C comme language de programmation : https://www.arduino.cc/en/software

## Les tutos
(for a future commit)

## Printed Part Count and Progress
| Part | Needed | Done |
|--|--|--|
| Wheel 3 Joint Servo Mount  Bogie joint p2 | 1 | 2 |
| Bogie joint p1 | 2 | 2 |
| Bogie joint p2 | 1 | 2 |
| Bogie Joint p2 - Mirrored | 1 | 0 |
| Base frame back bracket v2 | 1 | 0 |
| Base frame front and back bracket | (0) | 0 |
| Bracker for dif bar | 2 | 0 |
| Diff bar link | 2 | 0 |
| Differential Bar p1 | 2 | 0 |
| Differential Bar p2 | 1 | 0 |
| Electronics holder p1 | (1) | 0 |
| Electronics holder p2 | (1) | 0 |
| Rim and DC Motor Coupler | 6 | 0 |
| Rim | 6 | 6 |
| Rocker joint base | 2 | 0 |
| Rocker joint p2 | 1 | 0 |
| Rocker joint p2 - Mirrored | 1 | 0 |
| Rocker Joint | 2 | 0 |
| Servo coupler | 4 | 0 |
| Shaft Coupler - 5mm to 5mm (v2 for 5mm pulleys) | 4 | 0 |
| Wheel v.1 | 6 | 6 |
| Wheel 1 Joint | 4 | 0 |
| Wheel 1 Joint p2 | 4 | 0 |
| Wheel 1 Joint Servo Mount | 1 | 0 |
| Wheel 2 joint motor mount | 1 | 0 |
| Wheel 4 Joint Servo Mount - Mirrored | 1 | 0 |
| Wheel 5 joint motor mount - Mirrored | 1 | 0 |
| Wheel 6 Joint Servo Mount - Mirrored | 1 | 0 |
